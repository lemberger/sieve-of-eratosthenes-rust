<!--
This file is part of the concurrent Rust implementation
of the Sieve of Eratosthenes:
https://gitlab.com/lemberger/sieve-of-eratosthenes

SPDX-FileCopyrightText: 2020 Thomas Lemberger <https://thomaslemberger.com>

SPDX-License-Identifier: Apache-2.0
-->

# Concurrent Sieve of Eratosthenes

[![Apache 2.0 License](https://img.shields.io/badge/license-Apache--2-brightgreen.svg?style=flat)](https://www.apache.org/licenses/LICENSE-2.0)

This project shows a concurrent Rust implementation for computing prime numbers
with the [Sieve of Eratosthenes](https://en.wikipedia.org/wiki/Sieve_of_Eratosthenes).
Communication between threads is implemented through channels.

## Usage:

Run the program with
```bash
cargo run <max_number>
```

or build the project with `cargo build` and then run

```bash
./target/debug/sieve_of_eratosthenes <max_number>
```

Argument `<max_number>` is the maximum number to include in the computation.