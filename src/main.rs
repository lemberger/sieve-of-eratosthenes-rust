// This file is part of the concurrent Rust implementation
// of the Sieve of Eratosthenes:
// https://gitlab.com/lemberger/sieve-of-eratosthenes
//
// SPDX-FileCopyrightText: 2020 Thomas Lemberger <https://thomaslemberger.com>
//
// SPDX-License-Identifier: Apache-2.0

use std::io::Write;
use std::sync::mpsc;
use std::sync::mpsc::{Receiver, Sender};
use std::thread;
use structopt::StructOpt;

#[derive(Debug, StructOpt)]
struct Cli {
    max_number: i32,
}

fn generate_numbers(tx: Sender<i32>, max: i32) {
    for i in 2..max + 1 {
        tx.send(i).unwrap();
    }
}

fn filter(rx: Receiver<i32>, tx: Sender<i32>, number: i32) {
    for next in rx {
        if next % number != 0 {
            tx.send(next).unwrap();
        }
    }
}

fn generate_primes(max: i32, sender: Sender<i32>) {
    let (tx, mut rx): (Sender<i32>, Receiver<i32>) = mpsc::channel();

    thread::spawn(move || generate_numbers(tx, max));

    loop {
        let prime = match rx.recv() {
            Ok(num) => num,
            Err(_) => break,
        };
        sender.send(prime).unwrap();
        let (tx_next, rx_next): (Sender<i32>, Receiver<i32>) = mpsc::channel();
        thread::spawn(move || filter(rx, tx_next, prime));
        rx = rx_next;
    }
}

fn main() {
    let args = Cli::from_args();

    let (prime_tx, prime_rx): (Sender<i32>, Receiver<i32>) = mpsc::channel();
    thread::spawn(move || generate_primes(args.max_number, prime_tx));

    for prime in prime_rx {
        match writeln!(std::io::stdout(), "{}", prime) {
            Ok(_) => (),
            Err(_) => break,
        };
    }
}
